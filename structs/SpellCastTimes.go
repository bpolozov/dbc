package structs

type SpellCastTimes struct {
	ID 		 uint32 `gorm:"column entry"`
	Base 	 uint32 `gorm:"column base"`
	PetLevel uint32 `gorm:"column petlevel"`
	Minimum  uint32 `gorm:"column minimum"`
}


func (s SpellCastTimes) TableName() string {
	return "dbc_spell_cast_times"
}