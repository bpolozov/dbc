package structs

type SkillLineAbility struct {
	Id                       uint32 `json:"id" gorm:"column:id;primary_key;auto_increment:false"`
	SkillLine                uint32 `json:"skill_id" gorm:"column:skill_id"`
	Spell                    uint32 `json:"spell_id" gorm:"column:spell_id"`
	RaceMask                 uint32 `json:"race_mask" gorm:"column:race_mask"`
	ClassMask                uint32 `json:"class_mask" gorm:"column:class_maks"`
	ExcludeRace              uint32 `json:"excluderace" gorm:"column:exclude_race"`
	ExcludeClass             uint32 `json:"excludeclass" gorm:"column:exclude_class"`
	MinSkillLineRank         uint32 `json:"req_skill_value" gorm:"column:req_skill_value"`
	SupercededBySpell        uint32 `json:"superseded_by_spell" gorm:"column:superseded_by_spell"`
	AquireMethod             uint32 `json:"aquiremethod" gorm:"column:aquiremethod"`
	TrivialSkillLineRankHigh uint32 `json:"max_value" gorm:"column:max_value"`
	TrivialSkillLineRankLow  uint32 `json:"min_value" gorm:"column:min_value"`
	CharacterPoints1         uint32 `json:"characterpoints1" gorm:"column:characterpoints1"`
	CharacterPoints2         uint32 `json:"characterpoints2" gorm:"column:characterpoints2"`
	NumSkillUps              uint32 `json:"numskillups" gorm:"column:numskillups"`
}


func (s SkillLineAbility) TableName() string {
	return "dbc_skill_line_ability"
}