package structs


type ItemDisplayInfo struct {
	ID uint32
	ModelName1 string
	ModelName2 string
	ModelTexture1 string
	ModelTexture2 string
	InventoryIcon1 string
	GeosetGroup1 uint32
	GeosetGroup2 uint32
	GeosetGroup3 uint32
	Flags uint32
	SpellVisualID uint32
	GroupSoundIndex uint32
	HelmetGeosetVis1 uint32
	HelmetGeosetVis2 uint32
	Texture1 string
	Texture2 string
	Texture3 string
	Texture4 string
	Texture5 string
	Texture6 string
	Texture7 string
	Texture8 string
	ItemVisual uint32
}


func (s ItemDisplayInfo) TableName() string {
	return "dbc_item_display_info"
}