package structs

import (
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Mysql struct {
	Engine *gorm.DB
}

func (db *Mysql) Connect() {
	var err error
	db.Engine, err = gorm.Open("mysql", "mangos:mangos@tcp(127.0.0.1:3306)/world?charset=utf8")
	db.Engine.LogMode(true)
	if err != nil {
		log.Fatal(err)
	}
}

