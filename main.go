package main

import (
	"log"
	"os"
	"flag"
	"gitlab.com/bpolozov/dbc/structs"
	"io/ioutil"
	"fmt"
)

var FilePathIn  string
var FilePathOut string
var Load bool
var Dump bool
var DbcType string
var Mysql bool

func init() {
	flag.StringVar(&FilePathIn, "in", "", "file to read")
	flag.StringVar(&FilePathOut, "out", "", "file to dump")
	flag.BoolVar(&Load, "load", false, "load a *.dbc file to *.json")
	flag.BoolVar(&Dump, "dump", false, "load a *.json file to *.dbc")
	flag.StringVar(&DbcType, "type", "", "type of dbc file")
	flag.BoolVar(&Mysql, "mysql", false, "mysql")
}

func main() {
	flag.Parse()
	if Load && !Dump && FilePathOut != "" && FilePathIn != "" && DbcType != "" {
		inFile, err := os.Open(FilePathIn)
		if err != nil {
			log.Fatal(err)
		}

		dbc := structs.Dbc{}

		dbc.Init(inFile)

		DataJson := dbc.DbcToJson(DbcType)

		ioutil.WriteFile(FilePathOut, DataJson,0644)

	}
	if Dump && !Load && FilePathOut != "" && FilePathIn != "" && DbcType != "" {
		inFile, err := os.Open(FilePathIn)

		if err != nil {
			log.Fatal(err)
		}

		dbc := structs.Dbc{}
		data := dbc.DbcFromJson(inFile, DbcType)

		ioutil.WriteFile(FilePathOut,data,0644)
	}
	if Mysql && Load && FilePathIn != "" && DbcType != "" {
		db := structs.Mysql{}
		db.Connect()

		dbc := structs.Dbc{}
		inFile, err := os.Open(FilePathIn)
		if err != nil {
			log.Fatal(err)
		}
		dbc.Init(inFile)

		switch DbcType {
		case "Spell":
			db.Engine.CreateTable(structs.Spell{})

			data := dbc.DbcToObjects(DbcType)

			for i := 0; i < len(data); i++ {
				db.Engine.Table("dbc_spell").Create(data[i])
			}
		case "SpellIcon":
			db.Engine.CreateTable(structs.SpellIcon{})

			data := dbc.DbcToObjects(DbcType)

			for i := 0; i < len(data); i++ {
				db.Engine.Table("dbc_spell_icon").Create(data[i])
			}
		case "SkillLineAbility":
			db.Engine.CreateTable(structs.SkillLineAbility{})

			data := dbc.DbcToObjects(DbcType)

			for i := 0; i < len(data); i++ {
				db.Engine.Table("dbc_skill_line_ability").Create(data[i])
			}
		case "SpellCastTimes":
			db.Engine.CreateTable(structs.SpellCastTimes{})

			data := dbc.DbcToObjects(DbcType)

			for i := 0; i < len(data); i++ {
				db.Engine.Table("dbc_spell_cast_times").Create(data[i])
			}
		case "SpellRange":
			db.Engine.CreateTable(structs.SpellRange{})

			data := dbc.DbcToObjects(DbcType)

			for i := 0; i < len(data); i++ {
				db.Engine.Table("dbc_spell_range").Create(data[i])
			}
		case "ItemDisplayInfo":
			db.Engine.CreateTable(structs.ItemDisplayInfo{})

			data := dbc.DbcToObjects(DbcType)

			for i := 0; i < len(data); i++ {
				db.Engine.Table("dbc_item_display_info").Create(data[i])
			}
		case "ItemClass":
			db.Engine.CreateTable(structs.ItemClass{})

			data := dbc.DbcToObjects(DbcType)

			for i := 0; i < len(data); i++ {
				db.Engine.Table("dbc_item_class").Create(data[i])
			}
		case "ItemSubClass":
			db.Engine.CreateTable(structs.ItemSubClass{})

			data := dbc.DbcToObjects(DbcType)

			for i := 0; i < len(data); i++ {
				db.Engine.Table("dbc_item_sub_class").Create(data[i])
			}
		case "ItemSubClassMask":
			db.Engine.CreateTable(structs.ItemSubClassMask{})

			data := dbc.DbcToObjects(DbcType)

			for i := 0; i < len(data); i++ {
				db.Engine.Table("dbc_item_sub_class_mask").Create(data[i])
			}
		}

	}
	if Mysql && Dump && FilePathOut != "" && DbcType != "" {
		fmt.Println("HI")
		db := structs.Mysql{}
		db.Connect()

		dbc := structs.Dbc{}
		arr := make([]interface{},0)
		switch DbcType {
		case "Spell":
			data := []structs.Spell{}
			db.Engine.Find(&data)

			for _, i := range data {
				arr = append(arr, i)
			}

		case "SpellIcon":
			data := []structs.SpellIcon{}
			db.Engine.Find(&data)

			for _, i := range data {
				arr = append(arr, i)
			}
		case "SkillLineAbility":
			data := []structs.SkillLineAbility{}

			db.Engine.Find(&data)
			fmt.Println(data[0])
			fmt.Println(data[2])
			for _, i := range data {
				arr = append(arr, i)
			}
		case "SpellCastTimes":
			data := []structs.SpellCastTimes{}

			db.Engine.Find(&data)

			for _, i := range data {
				arr = append(arr, i)
			}
		case "SpellRange":
			data := []structs.SpellRange{}

			db.Engine.Find(&data)

			for _, i := range data {
				arr = append(arr, i)
			}
		case "ItemDisplayInfo":
			data := []structs.ItemDisplayInfo{}

			db.Engine.Find(&data)
			for _, i := range data {
				arr = append(arr, i)
			}
		case "ItemClass":
			data := []structs.ItemClass{}

			db.Engine.Find(&data)
			for _, i := range data {
				arr = append(arr, i)
			}
		case "ItemSubClass":
			data := []structs.ItemSubClass{}

			db.Engine.Find(&data)
			for _, i := range data {
				arr = append(arr, i)
			}
		case "ItemSubClassMask":
			data := []structs.ItemSubClassMask{}

			db.Engine.Find(&data)
			for _, i := range data {
				arr = append(arr, i)
			}

		}

		ioutil.WriteFile(FilePathOut,dbc.Dump(arr), 0644)


	}
}
