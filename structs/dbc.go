package structs

import (
	"os"
	"fmt"
	"log"
	"encoding/binary"
	"bytes"
	"math"
	"reflect"
	"encoding/json"
)

var FormatArratMap = map[string]interface{}{
	"Spell"				: func () []Spell 				{ return []Spell{} 				}(),
	"QuestInfo"			: func () []QuestInfo 			{ return []QuestInfo{} 			}(),
	"SkillLineAbility"	: func () []SkillLineAbility	{ return []SkillLineAbility{} 	}(),
	"SpellIcon"			: func () []SpellIcon	 		{ return []SpellIcon{} 			}(),
	"StableSlotPrices"	: func () []StableSlotPrices 	{ return []StableSlotPrices{} 	}(),
	"SkillLine"         : func () []SkillLine           { return []SkillLine{}          }(),
	"SpellCastTimes"    : func () []SpellCastTimes      { return []SpellCastTimes{}     }(),
	"SpellRange"        : func () []SpellRange          { return []SpellRange{}         }(),
	"ItemDisplayInfo"   : func () []ItemDisplayInfo     { return []ItemDisplayInfo{}    }(),
	"ItemClass"         : func () []ItemClass           { return []ItemClass{}          }(),
	"ItemSubClass"      : func () []ItemSubClass        { return []ItemSubClass{}       }(),
	"ItemSubClassMask"  : func () []ItemSubClassMask    { return []ItemSubClassMask{}   }(),
}

/*
var FormatMap = map[string]interface{}{
	"Spell"				: func () Spell 			{ return Spell{} 				}(),
	"QuestInfo"			: func () QuestInfo 		{ return QuestInfo{} 			}(),
	"SkillLineAbility"	: func () SkillLineAbility 	{ return SkillLineAbility{} 	}(),
	"SpellIcon"			: func () SpellIcon 		{ return SpellIcon{} 			}(),
	"StableSlotPrices"	: func () StableSlotPrices 	{ return StableSlotPrices{} 	}(),
}*/

type Dbc struct {

	magic           [4]byte // WDBC
	RecordCount     uint32
	FieldCount      uint32
	RecordSize      uint32  // row len in bytes
	StringBlockSize uint32  // len in bytes
	Records         []byte  // data before string block
	recordPosition  int
	StringRecords   []byte  // data from bin-records to end of file of C-strings
	stringPosition  int


}

func (dbc *Dbc) Init(f *os.File) {
	//log.Printf("Read Header")
	buff := make([]byte, 20)

	_, err := f.Read(buff)
	if err != nil {
		log.Fatal(err)
	}

	//log.Printf("%v", buff)

	pos := 4
	dbc.magic[0] = buff[0]
	dbc.magic[1] = buff[1]
	dbc.magic[2] = buff[2]
	dbc.magic[3] = buff[3]

	if  fmt.Sprintf("%s", dbc.magic) != "WDBC" {
		log.Fatalf("Invalid File Magic")
	}

	dbc.RecordCount = binary.LittleEndian.Uint32(buff[pos:pos+4])
	pos += 4
	dbc.FieldCount  = binary.LittleEndian.Uint32(buff[pos:pos+4])
	pos += 4
	dbc.RecordSize = binary.LittleEndian.Uint32(buff[pos:pos+4])
	pos += 4
	dbc.StringBlockSize = binary.LittleEndian.Uint32(buff[pos:pos+4])

	/*log.Printf("RecordCount -> %d FieldCount -> %d RecordSize -> %d StringBlockSize -> %d",
		dbc.RecordCount,
		dbc.FieldCount,
		dbc.RecordSize,
		dbc.StringBlockSize,
	)*/

	dbc.Records = make([]byte, dbc.RecordCount * (dbc.RecordSize))
	_, err = f.Read(dbc.Records)
	if err != nil {
		log.Fatal(err)
	}

	//fmt.Println(hex.Dump(dbc.Records))

	dbc.StringRecords = make([]byte, dbc.StringBlockSize)
	_, err = f.Read(dbc.StringRecords)
	if err != nil {
		log.Fatal(err)
	}

	//fmt.Println(hex.Dump(dbc.StringRecords))
}


func (dbc *Dbc) ReadUint32 () uint32 {
	//fmt.Println("Read Uint32", dbc.Records[dbc.recordPosition:dbc.recordPosition+4])
	u32 := binary.LittleEndian.Uint32(dbc.Records[dbc.recordPosition:dbc.recordPosition+4])
	dbc.recordPosition += 4
	return u32
}

func (dbc *Dbc) ReadFloat32 () float32 {
	//fmt.Println("Read Float32", dbc.Records[dbc.recordPosition:dbc.recordPosition+4])
	u32 := binary.LittleEndian.Uint32(dbc.Records[dbc.recordPosition:dbc.recordPosition+4])
	f32 := math.Float32frombits(u32)
	dbc.recordPosition += 4
	return f32
}

func (dbc *Dbc) ReadString () string {
	//fmt.Println("Read String (position)", dbc.Records[dbc.recordPosition:dbc.recordPosition+4])
	pointer := binary.LittleEndian.Uint32(dbc.Records[dbc.recordPosition:dbc.recordPosition+4])
	buff := bytes.Buffer{}
	if pointer != 0 {
		for {
			if dbc.StringRecords[pointer] == 0 {
				dbc.recordPosition += 4
				return fmt.Sprintf("%s", buff.Bytes())
			}
			buff.WriteByte(dbc.StringRecords[pointer])
			pointer++
		}
	}
	dbc.recordPosition += 4
	return ""
}


func (dbc *Dbc) Load (v interface{}) {
	val := reflect.ValueOf(v).Elem()
	for c := 0; c < val.NumField(); c++ {

		valueField := val.Field(c)

		switch valueField.Kind() {
		case reflect.Uint32:
			data := dbc.ReadUint32()
			valueField.Set(reflect.ValueOf(data))
		case reflect.String:
			data := dbc.ReadString()
			valueField.SetString(data)
		case reflect.Float32:
			data := dbc.ReadFloat32()
			valueField.Set(reflect.ValueOf(data))
		}
	}
}





func (dbc *Dbc) DbcToObjects(dbctype string) []interface{} {
	DataArr := make([]interface{}, 0)
	switch dbctype {
	case "SpellIcon":
		for i := 0; i < int(dbc.RecordCount); i++ {
			s := SpellIcon{}

			dbc.Load(&s)
			fmt.Println(s)
			DataArr = append(DataArr, s)

		}
	case "Spell":
		for i := 0; i < int(dbc.RecordCount); i++ {
			s := Spell{}

			dbc.Load(&s)
			fmt.Println(s)
			DataArr = append(DataArr, s)

		}
	case "SkillLineAbility":
		for i := 0; i < int(dbc.RecordCount); i++ {
			s := SkillLineAbility{}

			dbc.Load(&s)
			fmt.Println(s)
			DataArr = append(DataArr, s)
		}
	case "SkillLine":
		for i := 0; i < int(dbc.RecordCount); i++ {
			s := SkillLine{}

			dbc.Load(&s)
			fmt.Println(s)
			DataArr = append(DataArr, s)
		}
	case "SpellCastTimes":
		for i := 0; i < int(dbc.RecordCount); i++ {
			s := SpellCastTimes{}

			dbc.Load(&s)
			fmt.Println(s)
			DataArr = append(DataArr, s)
		}
	case "SpellRange":
		for i := 0; i < int(dbc.RecordCount); i++ {
			s := SpellRange{}

			dbc.Load(&s)
			fmt.Println(s)
			DataArr = append(DataArr, s)
		}
	case "ItemDisplayInfo":
		for i := 0; i < int(dbc.RecordCount); i++ {
			s := ItemDisplayInfo{}

			dbc.Load(&s)
			fmt.Println(s)
			DataArr = append(DataArr, s)
		}
	case "ItemClass":
		for i := 0; i < int(dbc.RecordCount); i++ {
			s := ItemClass{}

			dbc.Load(&s)
			fmt.Println(s)
			DataArr = append(DataArr, s)
		}
	case "ItemSubClass":
		for i := 0; i < int(dbc.RecordCount); i++ {
			s := ItemSubClass{}

			dbc.Load(&s)
			fmt.Println(s)
			DataArr = append(DataArr, s)
		}
	case "ItemSubClassMask":
		for i := 0; i < int(dbc.RecordCount); i++ {
			s := ItemSubClassMask{}

			dbc.Load(&s)
			fmt.Println(s)
			DataArr = append(DataArr, s)
		}

	}

	return DataArr
}



func (dbc *Dbc) DbcToJson(dbctype string) []byte {
	DataArr := dbc.DbcToObjects(dbctype)



	DataJson, ok := json.Marshal(DataArr)
	if ok != nil {
		log.Fatal(ok)
	}

	return DataJson
}

func (dbc *Dbc) DbcFromJson( file *os.File, dbctype string ) []byte {
	s, _ := file.Stat()

	buff := make([]byte, s.Size())

	file.Read(buff)
	file.Close()

	tmp := FormatArratMap[dbctype]


	if tmp != nil {
		log.Fatalf("Uknown format")
	}

	json.Unmarshal(buff, &tmp)

	val := reflect.ValueOf(tmp)

	arr := make([]interface{},0)

	switch val.Kind() {
	case reflect.Slice:
		for i := 0; i < val.Len(); i++ {
			arr = append(arr, val.Index(i))
		}
	default:
		log.Fatalf("json file isn't slice")
	}

	return dbc.Dump(arr)
}

func (dbc *Dbc) DbcFromObjects( data interface{}) []byte {

	val := reflect.ValueOf(data)

	arr := make([]interface{},0)

	switch val.Kind() {
	case reflect.Slice:
		for i := 0; i < val.Len(); i++ {
			arr = append(arr, val.Index(i))
		}
	default:
		log.Fatalf("data isn't slice")
	}
	return dbc.Dump(arr)
}

func (dbc *Dbc) Dump (v []interface{}) []byte {
	Data := bytes.Buffer{}
	Data.Write([]byte{'W','D','B','C'})						// Magic

	Data.Write(u32tb(uint32(len(v))))						// RecordCount

	for _, item := range v {
		item := reflect.ValueOf(item)
		if item.Kind() == reflect.Ptr {
			item = reflect.Indirect(item)
		}

		Data.Write(u32tb(uint32(item.NumField())))			// FieldCount

		for c := 0; c < item.NumField(); c++ {

			valueField := item.Field(c)

			switch valueField.Kind() {
			case reflect.Uint32:

				dbc.RecordSize += 4
			case reflect.String:
				dbc.RecordSize += 4
			case reflect.Float32:
				dbc.RecordSize += 4
			}
		}

		Data.Write(u32tb(uint32(dbc.RecordSize)))                // RecordSize
		break
	}

	binRecords    := bytes.Buffer{}
	stringRecords := bytes.Buffer{}
	stringRecords.WriteByte(0x00)

	for _, item := range v {
		val := reflect.ValueOf(item)

		if val.Kind() == reflect.Ptr {
			val = reflect.Indirect(val)
		}

		for c := 0; c < val.NumField(); c++ {
			valueField := val.Field(c)
			switch valueField.Kind() {
			case reflect.Uint32:
				x := valueField.Uint()
				binRecords.Write(u32tb(uint32(x)))
			case reflect.String:
				if len(stringRecords.Bytes()) == 0 {
					binRecords.Write(u32tb(1))
				}
				if valueField.String() != "" {
					binRecords.Write(u32tb(uint32(len(stringRecords.Bytes()))))
					stringRecords.Write(append([]byte(valueField.String()), 0x00))
				} else {
					binRecords.Write(u32tb(0))
				}
			case reflect.Float32:
				f := valueField.Float()
				binRecords.Write(u32tb(math.Float32bits(float32((f)))))
			default:
				fmt.Println(valueField.Kind())
			}
		}
	}


	Data.Write(u32tb(uint32(len(stringRecords.Bytes()))))
	Data.Write(binRecords.Bytes())
	Data.Write(stringRecords.Bytes())
	return Data.Bytes()
}


func u32tb (u32 uint32) []byte {
	buff := make([]byte,4)
	binary.LittleEndian.PutUint32(buff, u32)
	return buff
}
