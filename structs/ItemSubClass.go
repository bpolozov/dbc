package structs

type  ItemSubClass struct {
	Entry uint32
	SubClassID uint32
	PrerequisiteProficiency uint32
	PostrequisiteProficiency uint32
	Flags uint32
	DisplayFlags uint32
	WeaponParrySeq uint32
	WeaponReadySeq uint32
	WeaponAttackSeq uint32
	WeaponSwingSize uint32
	DisplayName1  string
	DisplayName2  string
	DisplayName3  string
	DisplayName4  string
	DisplayName5  string
	DisplayName6  string
	DisplayName7  string
	DisplayName8  string
	DisplayNameMask uint32
	VerboseName1  string
	VerboseName2  string
	VerboseName3  string
	VerboseName4  string
	VerboseName5  string
	VerboseName6  string
	VerboseName7  string
	VerboseName8  string
	VerboseNameMask uint32
}

func (s ItemSubClass) TableName() string {
	return "dbc_item_sub_class"
}