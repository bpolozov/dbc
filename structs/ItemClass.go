package structs

type  ItemClass struct {
	Entry uint32
	SubclassMapID uint32
	Flags uint32
	Classname1  string
	Classname2  string
	Classname3  string
	Classname4  string
	Classname5  string
	Classname6  string
	Classname7  string
	Classname8  string
	ClassnameMask uint32
}


func (s ItemClass) TableName() string {
	return "dbc_item_class"
}