package structs

type  ItemSubClassMask struct {
	Entry  uint32
	Mask   uint32
	Name1  string
	Name2  string
	Name3  string
	Name4  string
	Name5  string
	Name6  string
	Name7  string
	Name8  string
	NameMask uint32
}

func (s ItemSubClassMask) TableName() string {
	return "dbc_item_sub_class_mask"
}
