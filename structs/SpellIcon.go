package structs

type SpellIcon struct {
	Entry uint32 `json:"id" gorm:"entry"`
	TextureFilename string `json:"texturefilename" gorm:"TextureFilename"`
}

func (s SpellIcon) TableName() string {
	return "dbc_spell_icon"
}
