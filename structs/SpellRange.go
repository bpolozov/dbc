package structs


type SpellRange struct {
	ID       uint32
	RangeMin float32
	RangeMax float32
	Flags    uint32
	Name1    string
	Name2    string
	Name3    string
	Name4    string
	Name5    string
	Name6    string
	Name7    string
	Name8    string
	Namecrc  uint32
	Nameshrt1    string
	Nameshrt2    string
	Nameshrt3    string
	Nameshrt4    string
	Nameshrt5    string
	Nameshrt6    string
	Nameshrt7    string
	Nameshrt8    string
	Nameshrtcrc  uint32
}


func (s SpellRange) TableName() string {
	return "dbc_spell_range"
}