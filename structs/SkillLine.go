package structs

type SkillLine struct {
	ID              uint32 `json:"id" gorm:"id"`
	CategoryID      uint32 `json:"category_id" gorm:"category_id"`
	SkillCostsID    uint32 `json:"skill_cost_id" gorm:"skill_cost_id"`
	DisplayName1    string `json:"display_name1" gorm:"display_name1"`
	DisplayName2    string `json:"display_name2" gorm:"display_name2"`
	DisplayName3    string `json:"display_name3" gorm:"display_name3"`
	DisplayName4    string `json:"display_name4" gorm:"display_name4"`
	DisplayName5    string `json:"display_name5" gorm:"display_name5"`
	DisplayName6    string `json:"display_name6" gorm:"display_name6"`
	DisplayName7    string `json:"display_name7" gorm:"display_name7"`
	DisplayName8    string `json:"display_name8" gorm:"display_name8"`
	DisplayNameMask uint32 `json:"display_name_mask" gorm:"display_name_mask"`
	Description1    string `json:"description1" gorm:"description1"`
	Description2    string `json:"description2" gorm:"description2"`
	Description3    string `json:"description3" gorm:"description3"`
	Description4    string `json:"description4" gorm:"description4"`
	Description5    string `json:"description5" gorm:"description5"`
	Description6    string `json:"description6" gorm:"description6"`
	Description7    string `json:"description7" gorm:"description7"`
	Description8    string `json:"description8" gorm:"description8"`
	DescriptionMask uint32 `json:"description_mask" gorm:"description_mask"`
	SpellIconID     uint32 `json:"spell_icon_id" gorm:"spell_icon_id"`
}

func (s SkillLine) TableName() string {
	return "dbc_skill_line"
}
