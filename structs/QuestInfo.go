package structs

type QuestInfo struct {
	ID uint32     `json:"id"`
	Info1 string  `json:"info1"`
	Info2 string  `json:"info2"`
	Info3 string  `json:"info3"`
	Info4 string  `json:"info4"`
	Info5 string  `json:"info5"`
	Info6 string  `json:"info6"`
	Info7 string  `json:"info7"`
	Info8 string  `json:"info8"`
	Unknown uint32 `json:"Unknown"`
}

